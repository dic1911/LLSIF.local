#!/bin/bash

set -e

# Colors
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
PINK='\033[38;5;206m'
NC='\033[0m' # No Color


echo -e "${GREEN}[+] Installing needed packages!${NC}"
echo -e "${GREEN}[+] Make sure to press 'Y' when it prompts you a few times${NC}"
echo -e "${YELLOW}Press enter to continue!${NC}"
read

pkg update -y
pkg upgrade -y
pkg i openssl wget curl git tmux vim nano nodejs zip unzip screen -y

echo -e "${YELLOW}[-]${NC}"
echo -e "${GREEN}[+] Downloading the local server code repository!${NC}"
echo -e "${YELLOW}[-]${NC}"
git clone https://codeberg.org/dic1911/LLSIF.local && echo -e "${YELLOW}[-]${NC}";echo -e "${GREEN}[+] Downloaded correctly!${NC}"
cd 'LLSIF.local'

echo -e "${GREEN}[+] Installing server dependencies!${NC}"
echo -e "${YELLOW}[-]${NC}"
npm install

echo -e "${YELLOW}[-]${NC}"
echo -e "${GREEN}[+] Generating server_info.zip!${NC}"
echo -e "${YELLOW}[-]${NC}"
mkdir config
cp server_info.json config/server_info.json
zip -r http/server_info.zip config

echo -e "${YELLOW}[-]${NC}"
echo -e "${GREEN}[+] Setting up server settings!${NC}"
sed -i "s|server_info_url = ''|server_info_url = 'http://127.0.0.1:51376/server_info.zip'|g" data/game_consts.js
sed -i "s/server_info_size = 0/server_info_size = $(ls -l http | grep server_info.zip | awk '{print $5}')/g" data/game_consts.js

echo -e "${GREEN}[+] Setting up aliases!${NC}"
alias runserver="cd ~/LLSIF.local && node main"
alias updateserver="cd ~/LLSIF.local && git pull"
echo -e "${GREEN}[+] Updating .bashrc !${NC}"
touch ~/.bashrc
echo "alias runserver='cd ~/LLSIF.local && node main'" >> ~/.bashrc
echo "alias updateserver='cd ~/LLSIF.local && git pull'" >> ~/.bashrc
source ~/.bashrc

echo ""
echo -e "${GREEN}Done!${NC}"
echo -e "Run ${YELLOW}'runserver'${NC} to start the server or ${YELLOW}'updateserver'${NC} to update it!"
echo "Also, feel free to join the matrix server here:"
echo -e "${PINK}https://matrix.to/#/#ll-hax:m.sif.moe${NC}"
echo "Or the less secure discord server:"
echo -e "${GREEN}https://ll.sif.moe/discord${NC}"
echo ""
echo -e "${PINK}Nico nico nii~${NC}"
