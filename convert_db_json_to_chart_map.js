const fs = require('fs')

let live_settings = JSON.parse(fs.readFileSync('live_setting_m.json', 'utf8'))

let live_map = {}
for (set of live_settings) {
	live_map[set.live_setting_id] = set.notes_setting_asset
}


let filelist = [
    'normal_live_m.json',
    'special_live_m.json'
]

function convert(path) {
    let map = {}
    let json = JSON.parse(fs.readFileSync(path, 'utf8'))
    for (entry of json) {
        map[entry.live_difficulty_id] = live_map[entry.live_setting_id]
    }
    let mapName = path.replace('.json', 'ap.json')
    fs.writeFileSync(mapName, JSON.stringify(map))
    console.log('saved', mapName)
}

for (file of filelist) {
    convert(file)
}
