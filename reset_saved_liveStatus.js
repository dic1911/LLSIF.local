const fs = require('fs')


function getResetArr(arr, status) {
    let ret = []
    for (i in arr) {
        ret.push({
            live_difficulty_id: arr[i].live_difficulty_id,
            status: 1,
            hi_score: 0,
            hi_combo_count: 0,
            status: 0,
            clear_cnt: 0,
            achieved_goal_id_list: []
        })
    }
    return ret
}

var json = JSON.parse(fs.readFileSync('data/liveStatus.json', 'utf8'))

let newNormalLiveList = { normal_live_status_list: getResetArr(json.normal_live_status_list, 1) }
let newSpecialLiveList = { special_live_status_list: getResetArr(json.special_live_status_list, 1) }
let newTrainingLiveList = { training_live_status_list: getResetArr(json.training_live_status_list, 2) }


let ret = Object.assign({
    marathon_live_status_list: [],
    free_live_status_list: [],
    can_resume_live: false
}, newNormalLiveList)
ret = Object.assign(ret, newSpecialLiveList)
ret = Object.assign(ret, newTrainingLiveList)

fs.writeFileSync('data/liveStatus_empty.json', JSON.stringify(ret))

console.log("done")
