const fs = require('fs')

// used for replacing download target from JSON
const official_cdn_domain = 'http://dnw5grz2619mn.cloudfront.net'
const cdn_path = 'http://ll.sif.moe'
const microdl_dir = 'http://ll.sif.moe/v7/micro_download/<OS>/<VER>/'
const server_info_url = ''
const server_info_size = 0
exports.useNPPS4Structure = fs.existsSync("http/archive-root")

const workaroundUnitBatchDL = true
const osToLowerCase = true

const gachaPools = JSON.parse(fs.readFileSync('static/main.php-api/partial/secretbox.all.result.json', 'utf8'))
const normalChartMap = JSON.parse(fs.readFileSync('static/normal_live_map.json', 'utf8'))
const specialChartMap = JSON.parse(fs.readFileSync('static/special_live_map.json', 'utf8'))
const chartRankMap = JSON.parse(fs.readFileSync('static/live_setting_map.json'))
const liveGoalMap = JSON.parse(fs.readFileSync('static/live_goal_reward_map.json'))
const museum_info_txt = fs.readFileSync('static/museum_info.json', 'utf8')
const scenarioList = JSON.parse(fs.readFileSync('static/scenario/scenario_m.json', 'utf8'))
const subscenarioList = JSON.parse(fs.readFileSync('static/card_navi_ids.json', 'utf8'))
const multiUnitScenarioList = JSON.parse(fs.readFileSync('static/scenario/multi_unit_scenario_open_m.json', 'utf8'))
const multiUnitScenarioMap = JSON.parse(fs.readFileSync('static/scenario/multi_unit_scenario_map.json', 'utf8'))
const units = JSON.parse(fs.readFileSync('static/all_units.json'))
const availableAwardRanges = [
    [1, 572],
	[1300, 1300],
	[10000, 10011],
	[999001, 999030]
]
const availableAwardBlacklists = [
    [455, 456, 457, 458, 475, 477, 478],
	[],
	[],
	[999007, 999015]
]

const diffNames = [
	"EASY",
	"NORMAL",
	"HARD",
	"EXPERT",
	"MASTER",
	"MASTER"
]

const gachaItemIds = [
	1, 5, 23, 48, 49,
	50, 61, 62, 63, 168,
	193, 207, 256, 272, 349,
	353, 380, 382, 422, 1000,
	1200
]

exports.diffNames = diffNames
exports.gachaPools = gachaPools
exports.normalChartMap = normalChartMap
exports.specialChartMap = specialChartMap
exports.chartRankMap = chartRankMap
exports.liveGoalMap = liveGoalMap
exports.availableAwardBlacklists = availableAwardBlacklists
exports.availableAwardRanges = availableAwardRanges
exports.gachaItemIds = gachaItemIds
exports.museum_info_txt = museum_info_txt
exports.scenarioList = scenarioList
exports.subscenarioList = subscenarioList
exports.multiUnitScenarioList = multiUnitScenarioList
exports.multiUnitScenarioMap = multiUnitScenarioMap
exports.units = units

exports.official_cdn_domain = official_cdn_domain
exports.cdn_path = cdn_path
exports.microdl_dir = microdl_dir
exports.server_info_url = server_info_url
exports.server_info_size = server_info_size
exports.osToLowerCase = osToLowerCase
exports.workaroundUnitBatchDL = workaroundUnitBatchDL
