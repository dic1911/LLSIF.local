const fs = require('fs')
const game_consts = require('../data/game_consts')

var maintenance = true
var use_cache_for_mainapi = true
const rel_info = '[{"id":423,"key":"UDKkj/dmBRbz+CIB+Ekqyg=="},{"id":1870,"key":"Lckl38UoH8CfOMqMSmMYsA=="},{"id":1871,"key":"acAmAWyPOCrO+R5qY9UTtQ=="},{"id":1872,"key":"LaLzU62pKnTftSEGFhMqfA=="},{"id":1873,"key":"wiaaGZSJexvY0u4poRrGSw=="},{"id":1874,"key":"T18sDsU+81wLXTjCURNxJw=="}]'

var migrationStatusPath = 'data/migration.json'
exports.migrationStatus = (fs.existsSync(migrationStatusPath) ? JSON.parse(fs.readFileSync(migrationStatusPath)) : {})
exports.migrated = function (name) {
	exports.migrationStatus[name] = true
	fs.writeFileSync(migrationStatusPath, JSON.stringify(exports.migrationStatus))
}

// user_info
const userInfoPath = 'data/userInfo.json'
const extraUserInfoPath = 'data/userInfo_extra.json'
var userInfo = JSON.parse(fs.readFileSync(userInfoPath, 'utf8'))
var extraUserInfo = JSON.parse(fs.readFileSync(extraUserInfoPath, 'utf8'))
function saveUserInfo(info) {
	userInfo = info
	exports.userInfo = info
	fs.writeFileSync(userInfoPath, JSON.stringify(info))
}
function saveExtraUserInfo(info) {
	extraUserInfo = info
	exports.extraUserInfo = info
	fs.writeFileSync(extraUserInfoPath, JSON.stringify(info))
}

let _classSystem = JSON.parse(fs.readFileSync('static/class_system.json', 'utf8'))
_classSystem.rank_info.before_class_rank_id = extraUserInfo.rank
_classSystem.rank_info.after_class_rank_id = extraUserInfo.rank
const classSystem = JSON.stringify(_classSystem)

// live stuff
var currentDeckId = 1
var currentChartFile = 'Live_s2044.json'

// _unitAll = real storage, unitAll = for responses
const unitAllPath = 'data/unitAll.json'
var _unitAll = JSON.parse(fs.readFileSync(unitAllPath, 'utf8'))
var unitAll = {}

function refreshUnitAll() {
	let newList = {
		active: [],
		waiting: []
	}

	for (entries of Object.entries(_unitAll.active)) {
		newList.active.push(entries[1])
	}
	unitAll = newList
	exports.unitAll = newList
}

function saveUnitAll() {
	fs.writeFileSync(unitAllPath, JSON.stringify(_unitAll))
}

function updateUnitDisplayRank(id, drank) {
	_unitAll.active[id].display_rank = drank
	refreshUnitAll()
	saveUnitAll()
}

function getUnitId(card_id) {
	return _unitAll.active[card_id].unit_id
}

function getUnit(card_id) {
	return _unitAll.active[card_id]
}

function getRandomActiveUnit() {
	let id_list = Object.keys(_unitAll.active)
	let id = id_list[Math.floor(Math.random() * id_list.length)]
	return _unitAll.active[id]
}

refreshUnitAll()

const deckInfoPath = 'data/deck.json'
var deckInfo = JSON.parse(fs.readFileSync(deckInfoPath, 'utf8'))
for (deck of deckInfo) {
	if (deck.main_flag) {
		currentDeckId = deck.unit_deck_id
		break
	}
}

const liveStatusPath = 'data/liveStatus.json'
var liveStatus = JSON.parse(fs.readFileSync(liveStatusPath, 'utf8'))
var cache = {
	main_php_api: {}
}

function updateDeckInfo(newDeckInfo) {
	deckInfo = newDeckInfo
	exports.deckInfo = deckInfo
	fs.writeFileSync(deckInfoPath, JSON.stringify(newDeckInfo))
	console.log('updated deck info')
}

function getLiveRecord(type, id) {
	if (type == 1)
		targetList = liveStatus.normal_live_status_list
	else if (type == 2)
		targetList = liveStatus.special_live_status_list
	else if (type == 3)
		targetList = liveStatus.training_live_status_list
	else return

	for (i in targetList) {
		if (targetList[i].live_difficulty_id == id) {
			console.log('found record')
			if (targetList[i].clear_cnt == 0 && type != 2) {
				let candidate = getLiveRecord(2, id)
				if (candidate != undefined && candidate.clear_cnt > targetList[i].clear_cnt) {
					console.log('using second result')
					return candidate
				}
			}
			return targetList[i]
		}
	}
	console.error('found no record')
}

// 1: normal, 2: special, 3: training
function updateLiveStatus(type, id, diff, score, combo, rank_info, rank_map) {
	let targetList = undefined
	if (type == 1)
		targetList = liveStatus.normal_live_status_list
	else if (type == 2)
		targetList = liveStatus.special_live_status_list
	else if (type == 3)
		targetList = liveStatus.training_live_status_list
	else return {success: false, new_record: false, hi_score: 0}

	for (i in targetList) {
		if (targetList[i].live_difficulty_id == id) {
			let updated = false
			let newRecord = false
			let hiScore = 0
			console.log("found liveStatus entry, updating..")
			console.log("old entry", targetList[i])
			if (targetList[i].hi_score < score) {
				targetList[i].hi_score = score
				updated = true
				newRecord = true
				hiScore = score
			} else {
				hiScore = targetList[i].hi_score
			}
			
			if (targetList[i].hi_combo_count < combo) {
				targetList[i].hi_combo_count = combo
				updated = true
			}
			
			targetList[i].clear_cnt += 1
			let map = game_consts.liveGoalMap[id]
			let clearRank = 1

			console.log('live status, updated:', updated, ', map exists:', map != undefined, ', cache exists:', rank_info.goal_list_cache != undefined)
			if (updated) {
				if (map != undefined) {
					for (let ii = (rank_info.score - 1); ii < map.score.length; ii++) {
						if (targetList[i].achieved_goal_id_list.indexOf(map.score[ii]) == -1) {
							targetList[i].achieved_goal_id_list.push(map.score[ii])
						}
					}
					for (let ii = (rank_info.combo - 1); ii < map.combo.length; ii++) {
						if (targetList[i].achieved_goal_id_list.indexOf(map.combo[ii]) == -1) {
							targetList[i].achieved_goal_id_list.push(map.combo[ii])
						}
					}
				} else if (rank_info.goal_list_cache != undefined) {
					targetList[i].achieved_goal_id_list = rank_info.goal_list_cache
				}
			}

			if (map != undefined) {
				for (var ii = 0; ii < 4; ii++) {
	                                if (targetList[i].clear_cnt >= rank_map.clear_rank[ii]) {
	                                        clearRank = (ii + 1)
	                                        break
	                                }
	                                if (ii == 3)
	                                        clearRank = 5
	                        }
				for (let ii = (clearRank - 1); ii < map.clear.length; ii++) {
					if (targetList[i].achieved_goal_id_list.indexOf(map.clear[ii]) == -1) {
						targetList[i].achieved_goal_id_list.push(map.clear[ii])
					}
				}
			}

			console.log("new entry", targetList[i])
			exports.liveStatus = liveStatus
			if (type == 3) {
				updateLiveStatus(2, id, diff, score, combo, rank_info, rank_map)
			}

			storeActivity(14, game_consts.chartRankMap[exports.currentChartFile].track_name + " - " + game_consts.diffNames[diff - 1] + " がクリアしました, スコア: " + score + ", MAXコンボ: " + combo, Date().toString())
			return {success: true, new_record: newRecord, hi_score: hiScore}
		}
	}
	if (type == 1) {
		Object.assign(rank_info, {goal_list_cache: targetList[i].achieved_goal_id_list})
		return updateLiveStatus(2, id, diff, score, combo, rank_info, rank_map)
	}
	return {success: false, new_record: false, hi_score: 0}
}

function saveLiveStatus() {
	fs.writeFileSync(liveStatusPath, JSON.stringify(liveStatus))
}

// user activity
const activityLogPath = 'data/activityLog.json'
var activityLog = (fs.existsSync(activityLogPath) ? JSON.parse(fs.readFileSync(activityLogPath)) : [])

function storeActivity(t, msg) {
	activityLog.push({
		type: t,
		message: msg,
		time: Date().toString()
	})
	fs.writeFileSync(activityLogPath, JSON.stringify(activityLog))
	exports.activityLog = activityLog
}

storeActivity(8, "LLSIF @ HOME へようこそ!", Date().toString())

exports.use_cache_for_mainapi = use_cache_for_mainapi
exports.currentChartFile = currentChartFile
exports.currentDeckId = currentDeckId
exports.rel_info = rel_info
exports.cache = cache
exports.maintenance = maintenance
exports.liveStatusPath = liveStatusPath
exports.liveStatus = liveStatus
exports.updateLiveStatus = updateLiveStatus
exports.saveLiveStatus = saveLiveStatus
exports.getLiveRecord = getLiveRecord
exports.deckInfo = deckInfo
exports.updateDeckInfo = updateDeckInfo
exports.userInfo = userInfo
exports.extraUserInfo = extraUserInfo
exports.saveUserInfo = saveUserInfo
exports.saveExtraUserInfo = saveExtraUserInfo
exports.classSystem = classSystem
exports.unitAll = unitAll
exports.updateUnitDisplayRank = updateUnitDisplayRank
exports.saveUnitAll = saveUnitAll
exports.getUnitId = getUnitId
exports.getUnit = getUnit
exports.getRandomActiveUnit = getRandomActiveUnit
exports.activityLog = activityLog
exports.storeActivity = storeActivity
