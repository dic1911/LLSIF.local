const fs = require('fs')

const common_data = require('./common')
const game_consts = require('./game_consts')

let clearRankData = [
	[20, 10, 5, 1],
	[40, 20, 10, 5],
	[100, 40, 20, 10],
	[120, 60, 30, 15],
	[150, 60, 30, 15]
]
clearRankData.push(clearRankData[4])

function fixCompletedClearGoals() {
	let diffIdToChart = Object.assign(game_consts.normalChartMap, game_consts.specialChartMap)
	let data = common_data.liveStatus
	let handleList = function (list) {
		for (i in list) {
			let chart = list[i]
			let chartFile = diffIdToChart[chart.live_difficulty_id]
			let rankMap = game_consts.chartRankMap[chartFile]
			let goals = game_consts.liveGoalMap[rankMap]
			// clear rank
			let clearRank = 1
			for (var ii = 0; ii < 4; ii++) {
				if (chart.clear_cnt >= clearRankData[rankMap.difficulty - 1][ii]) {
					clearRank = (ii + 1)
					break
				}
				if (ii == 3)
					clearRank = 5
			}
			// push completed goals into array
			if (goals != undefined) {
				for (let ii = (clearRank - 1); ii < goals.clear.length; ii++) {
					if (chart.achieved_goal_id_list.indexOf(goals.clear[ii]) == -1) {
						chart.achieved_goal_id_list.push(goals.clear[ii])
					}
				}
			}
			list[i] = chart
		}
		return list
	}
	data.normal_live_status_list = handleList(data.normal_live_status_list)
	data.special_live_status_list = handleList(data.special_live_status_list)
	data.training_live_status_list = handleList(data.training_live_status_list)
	fs.writeFileSync(common_data.liveStatusPath, JSON.stringify(data))
}

exports.run = function () {
	if (common_data.migrationStatus.clear_goal_fixed == undefined) {
		console.log("migrating data for clear count goal")
		fixCompletedClearGoals()
		common_data.migrated('clear_goal_fixed')
	}
}
