const fs = require('fs')
const path = require('path')

const util = require('./handler/util')

const argv = process.argv
const argc = argv.length

if (argc < 4 || !fs.existsSync(argv[2] + "/liveStatus.json") || !fs.existsSync(argv[3])) {
	console.log("Usage: node " + argv[1].split(path.sep).reverse()[0] + " <path contains liveStatus.json> <main.php/api response body dump>")
	process.exit(1)
}

const savePath = argv[2]

var handledUnitAll = false

function main() {
	let sourcePath = argv[3]
	let sourceData = JSON.parse(fs.readFileSync(sourcePath)).response_data

	for (data of sourceData) {
		if (data.result != undefined && data.result.normal_live_status_list != undefined) {
			handleLiveStatus(data.result)
		} else if (data.result != undefined && data.result.active != undefined) {
			handleUnitAll(data.result)
		} else if (data.result != undefined && data.result.user != undefined && data.result.user.unit_owning_user_id != undefined) {
			handleNavi(data.result)
		}
	}
}

function handleNavi(data) {
	if (!handledUnitAll) {
		console.log("not setting navigator/partner cuz unitAll not merged to prevent crash")
	}
	let extraUserInfoPath = savePath + '/userInfo_extra.json'
	let extraUserInfo = JSON.parse(fs.readFileSync(extraUserInfoPath))
	let navi_id = data.user.unit_owning_user_id
	extraUserInfo.navi_id = navi_id
	fs.writeFileSync(extraUserInfoPath, JSON.stringify(extraUserInfo))
	console.log('set navi to ', navi_id)
}

function handleUnitAll(data) {
	let t0 = util.timestamp()
	let currentSavePath = savePath + '/unitAll.json'
	if (!fs.existsSync(currentSavePath)) {
		console.log("skipping merging unitAll because target file doesn't exist")
		return
	}
	let currentSave = JSON.parse(fs.readFileSync(currentSavePath))
	console.warn("merging active members & overwriting members in waiting room")
	for (card of data.active) {
		for (i in currentSave.active) {
			if (card.unit_owning_user_id == currentSave.active[i].unit_owning_user_id) {
				for (prop of Object.entries(card)) {
					if (prop[0] == "unit_id" || prop[0] == "insert_date") {
						currentSave.active[i][prop[0]] = prop[1]
					} else if (prop[0].indexOf("is_") == 0 && prop[0].indexOf("_max") > -1) {
						currentSave.active[i][prop[0]] = currentSave.active[i][prop[0]] || prop[1]
					} else {
						currentSave.active[i][prop[0]] = ((currentSave.active[i][prop[0]] > prop[1]) ? currentSave.active[i][prop[0]] : prop[1])
					}
				}
				break
			}
		}
		currentSave.active[card.unit_owning_user_id] = card
	}
	for (card of data.waiting) {
		currentSave.waiting[data.waiting.unit_owning_user_id] = card
	}
	fs.writeFileSync(currentSavePath, JSON.stringify(currentSave))
	handledUnitAll = true
	console.log("unitAll merged in", util.timestamp() - t0, "secs")
}

function handleLiveStatus(data) {
	let t0 = util.timestamp()
	let currentSave = JSON.parse(fs.readFileSync(savePath + '/liveStatus.json'))
	let sourceLists = [data.normal_live_status_list, data.special_live_status_list, data.training_live_status_list]
	let handleList = function (x) {
		for (i in x) {
			let found = false
			for (sourceList of sourceLists) {
				for (sourceData of sourceList) {
					if (x[i].live_difficulty_id == sourceData.live_difficulty_id) {
						x[i].hi_score = (x[i].hi_score > sourceData.hi_score) ? x[i].hi_score : sourceData.hi_score
						x[i].hi_combo_count = (x[i].hi_combo_count > sourceData.hi_combo_count) ? x[i].hi_combo_count : sourceData.hi_combo_count
						x[i].clear_cnt = x[i].clear_cnt + sourceData.clear_cnt
						for (goal of sourceData.achieved_goal_id_list) {
							if (x[i].achieved_goal_id_list.indexOf(goal) == -1) {
								x[i].achieved_goal_id_list.push(goal)
							}
						}
						found = true
						break
					}
					if (found) break
				}
				if (found) break
			}
			if (!found) {
				console.log("data for live_difficulty_id:", x[i].live_difficulty_id, "isn't found :(")
			}
		}
		return x
	}
	currentSave.normal_live_status_list = handleList(currentSave.normal_live_status_list)
	currentSave.special_live_status_list = handleList(currentSave.special_live_status_list)
	currentSave.training_live_status_list =  handleList(currentSave.training_live_status_list)
	fs.writeFileSync(savePath + '/liveStatus.json', JSON.stringify(currentSave))
	console.log("liveStatus merged in", util.timestamp() - t0, "secs")
}



main()
