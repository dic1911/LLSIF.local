# LLSIF @ Home

---

## Intro

This is a barebone single player server for LLSIF, expect things might be broken or causes the game to crash, gameplay is mostly fine though


## Checklist

 - [x] Login
 - [x] Deck (Set deck name and member)
 - [x] Gameplay
 - [x] Award (Note. Need to set rank value manually for different rank award)
 - [x] Home background
 - [x] Profile (Partial, character/card bonding rank not implemented)
 - [x] Album
 - [x] Story
 - [x] Gacha (definitely wrong data & behavior, randomly picked from one of the active member/unit)
 - [x] Memories Gallery
 - [x] Friend list (static empty list - it's a local server anyway)
 - [x] Mission (static empty list)
 - [x] Friend activity (in-ram-only live record history)
 - [ ] Account leveling, experience calculation... etc

Note. Unchecked item means it either crashes or non-functional



## Setup

### Android - Termux
#### Fast method:

(This guide assumes you've installed the patched client)

Before this, install the patched SIF client (Check `doc/client_android.md` for instruction if you don't have pre-patched client)

  1. Install Termux (I recommand to download from [F-Droid](https://f-droid.org/en/packages/com.termux/), [direct link for latest release atm](https://f-droid.org/repo/com.termux_118.apk)) and open it
  2. Download this file into your phone's downloads folder:
    [server_info.json file to download](https://codeberg.org/dic1911/LLSIF.local/raw/branch/master/server_info.json).
  3. Run these 2 commands inside of Termux:
    `wget https://ll.sif.moe/android`
    `bash android`
  4. Follow the instructions after the command finishes. It will say 'Done!' After that, launch the game, let it update. Then it will fail afterwards. We need to tell it what the new server is (your phone is the server now).
  5. After this is done, you should be able to launch the game and it should just work. Ask in the discord for help! 
    `runserver` to run the server code, and `updateserver` to update it.

#### More detail:
 - Install patched SIF client (Check `doc/client_android.md` for instruction if you don't have pre-patched client)
 - Install Termux (I recommand to download from [F-Droid](https://f-droid.org/en/packages/com.termux/), [direct link for latest release atm](https://f-droid.org/repo/com.termux_118.apk)) and open it
 - Install Git and NodeJS - Run `pkg update -y; pkg upgrade -y;pkg i openssl nodejs git -y` (it will prompt you with something, just do `Y`)
 - Clone this repo - Run `git clone https://codeberg.org/dic1911/LLSIF.local`
 - Install dependencies - Run `cd LLSIF.local` then `npm install`
 - (Optional) Edit `data/game_consts.js` to custom server - Thanks caret for providing a public one
   - `cdn_path` points to HTTP accessible path where you extract `server-content.7z`
   - `microdl_dir` points to HTTP accessible path where you extract `server-micro_download.7z`
 - (Optional) Provide patched `server_info.json` from server side
   - Make a zip archive containing a patched `server_info.json` in config folder
   - a. If you're serving assets from this local server:
     - Name the zip archive `server_info` and put it inside `http` folder
   - b. If you're still using assets from other server:
     - Edit `game_consts.js` and set `server_info_url` to where you could serve the patched `server_info` archive, and set `server_info_size` to what size `ls -l` would report
 - Run the server - Run `node main`
 - Run the game and download assets
 - After the game restarts itself you should encounter connection error
 - Force stop the game
 - Overwrite the following two files with the `server_info.json` in this repo
   - `/sdcard/Android/data/klb.android.loveliveps/files/external/config/server_info.json`
   - `/sdcard/Android/data/klb.android.loveliveps/files/install/config/server_info.json`
   - PS. `klb.android.loveliveps` is my cloned package, replace with your own package name if you're using different package
   - PPS. If you're on newer Android versions(such as Android 13), you may need to install modded build with permission fix
     - Extended steps: Force stop and launch, force stop again, and replace the files afterwards
     - Tip. Not every file manager supports operation under `/sdcard/Android/data`, use [FV File Manager](https://play.google.com/store/apps/details?id=com.folderv.file) if you don't know what to use
 - Launch the game
 - Profit!


### iOS

 - Setup the server on other devices
   - (If anyone ever succeed on hosting this local server on iOS please open a PR with instrustions)
 - a. Edit `data/game_consts.js`, make all the http url use https instead
 - b.1 Edit `data/game_consts.js`, only set official cdn url to use https
 - b.2 Edit info.plist and add the following content:
```
<key>NSAppTransportSecurity</key>
<dict>
  <key>NSAllowsArbitraryLoads</key>
      <true/>
</dict>
```
 - Patch `server_info.json` in assets server
   - Path: `/v7/iphone/59.4/update/99/1/5f0b3f94c768d6eeb87fa481480b95d3` according to kotori8823


### PC - Linux/Windows/MacOS

 - Install NodeJS as how you should do it for your OS
 - Other steps is basically the same, except that you'll need to patch `server_info.json` by yourself with `honoka2`
 - Note that you can extract the assets to `http` folder of this repo like the tree graph below and set:

 `cdn_path` to `'http://<YOUR IP>:51376/'`
 `microdl_dir` to `'http://<YOUR IP>:51376/micro_download/<OS>/<VER>/'`

```
LLSIF.local
└──http
    ├── micro_download
    │   ├── android
    │   └── iphone
    └── v7
        ├── android
        └── iphone
```


## Updating the server

 - Run the following commands one by one
```
git stash
git pull
# note that it's possible you could get conflicts at this step, solve the conflicts before you run the server
git stash apply
# (optional) after solving the conflict, run the server and make sure things are working fine, drop the stash (the backup of your save)
git stash drop
```
 - If you don't know how to solve the conlicts, feel free to ask in Discord


## Migrating data from previous account

 - Install WireShark and open the .pcap file for your account
 - Click through the menu, "File" -> "Export Objects" -> "HTTP..."
 - Search for "api" and look for the biggest one, which should have the content type "application/json"
 - Click on the packet in list and click save to export
 - Use `mergeAccount.js` in this repo to merge data
   - Example: `node mergeAccount.js ./data ./api`
   - Where `./data` is the location for where `liveStatus.json` located and `./api` is the path for the dumped output from WireShark


## Known issues

 - Initial batch download and unit data batch download in settings could be buggy, set `workaroundUnitBatchDL` to different value in `data/game_consts.js` if the behavior isn't expected
 - Some charts are missing, please open pull request if you can get those charts
 - Check "Issues" page for more

## Credits


This is made possible by many members from the LL Hax community

- HenTaku/030 - for most of the spaghetti code you see here
- tungnotpunk - for server side stuff and assets
- AuahDark - for asset decryption, client patches, and asset scraping
- Salaron - for chart db
- Atton\_Risk - for git server
- caret - for public asset server and script process
- Sarayalth - for making the script process easier
- llsif.moe - for card data
