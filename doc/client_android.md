# Patching client for Android


## Requirement

 - apktool
 - apksigner
 - Hex Editor (I uses HxD)
 - [libhonoka](https://github.com/DarkEnergyProcessor/libhonoka)
 - md5sum
 - zipalign

PS. `apksigner` and `zipalign` is part of Android SDK

 ## Instruction

 0. Obtain official apk/apks - use [AntiSplit](https://forum.xda-developers.com/t/antisplit-g2-mod-debugged.4303349/) if you have apks(split apk)
 1. Decompile official APK file with apktool (`apktool d LLSIF_JP_9.11.apk`)
 2. Open `AndroidManifests.xml`, search for `extractNativeLib` attribute and remove it
 3. Patch public key in `lib/<arch>/libGame.so` - open it with hex editor, search for `BEGIN PUBLIC KEY` and replace the whole key with the following
```
-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDE0RNd6047aeBirzVb61DolatY
YWpaEUIPugOIkobHDc9qVR5iliMLyC0ErXO1siLBwN+U3zaDVOa5uhXbiS7uYq5c
cpxComxTnZtcn/b+mKDpYWLaC0Gv7UoiT8rpNqN3Vko645usz9OFc4VciijsHGRP
XmmmoP6qykfI/vba8wIDAQAB
-----END PUBLIC KEY-----
```
 4. Unzip `config/server_info.json` in `assets/AppAssets.zip`
 5. Decrypt `server_info.json` (`honoka2 server_info.json`)
 6. Edit the json file to match `localhost:51376` or whatever endpoint you are about to serve the server
 7. Encrypt the json file back with `honoka2 -e -j server_info.json`
 8. Replace `server_info.json` in `assets/AppAssets.zip`
 9. Get md5sum of `assets/AppAssets.zip` and replace the old md5sum in `assets/version`
 10. Run `apktool b .` to build modified APK
 11. Do zipalign (`zipalign -f 4 dist/<apk name> LLSIF_patched.apk`)
 12. Sign the apk (`apksigner sign -ks <keystore path> LLSIF_patched.apk`)
 13. Install & Profit!
