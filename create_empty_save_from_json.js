const fs = require('fs')

const liveStatusSrc = [
    'static/normal_live_m.json',
    'static/special_live_m.json'
]

var liveStatusOutput = {
    normal_live_status_list: [],
    special_live_status_list: [],
    marathon_live_status_list: [],
    free_live_status_list: [],
    can_resume_live: false
}

function liveStatusEntry(id) {
    return {
        "live_difficulty_id": id,
        "status": 1,
        "hi_score": 0,
        "hi_combo_count": 0,
        "clear_cnt": 0,
        "achieved_goal_id_list": []
    }
}

for (src of liveStatusSrc) {
    json = JSON.parse(fs.readFileSync(src, 'utf8'))
    if (src.indexOf('normal_live') > -1) {
        for (e of json) {
            liveStatusOutput.normal_live_status_list.push(liveStatusEntry(e.live_difficulty_id))
        }
    } else {
        for (e of json) {
            liveStatusOutput.special_live_status_list.push(liveStatusEntry(e.live_difficulty_id))
        }
    }
}

console.log("done")
console.log("normal_live_status_list.length:", liveStatusOutput.normal_live_status_list.length)
console.log("special_live_status_list.length:", liveStatusOutput.special_live_status_list.length)

fs.writeFileSync('data/liveStatus.json', JSON.stringify(liveStatusOutput))