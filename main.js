const express = require('express')
const app = express()
const fs = require('fs');

const multer = require('multer');
const mparser = multer()

const common_data = require('./data/common')
const migration = require('./data/migration')

const account = require('./handler/account')
const customize = require('./handler/customize')
const download = require('./handler/download')
const gacha = require('./handler/gacha')
const user = require('./handler/user')
const live = require('./handler/live')
const mainapi = require('./handler/main')
const misc = require('./handler/misc')
const notice = require('./handler/notice')
const story = require('./handler/story')
const util = require('./handler/util')


const ip = '0.0.0.0'
const port = 51376

app.use(express.static('http'))

app.use(function (req, res, next) {
	console.log(req.method, ":", req.url)
	next()
})

app.post('/main.php/login/authkey', mparser.none(), account.login_authkey)
app.post('/main.php/login/startUp', account.login_startUp)
app.post('/main.php/login/login', mparser.none(), account.login_login)
app.post('/main.php/login/topInfo', account.login_topInfo)
app.post('/main.php/tos/*', account.tos)
app.post('/main.php/gdpr/*', account.gdpr)
app.post('/main.php/tutorial/*', account.tutorial)
app.post('/main.php/lbonus/execute', account.lbonus_execute)
app.post('/main.php/handover/kidStatus', account.handover_kidStatus)
app.post('/main.php/friend/list', mparser.none(), account.friend_list)

app.post('/main.php/user/userInfo', user.userInfo)
app.post('/main.php/user/changeName', mparser.none(), user.changeName)
app.post('/main.php/user/changeNavi', mparser.none(), user.changeNavi)

app.post('/main.php/download/update', mparser.none(), download.update)
app.post('/main.php/download/event', mparser.none(), download.event)
app.post('/main.php/download/additional', mparser.none(), download.additional)
app.post('/main.php/download/batch', mparser.none(), download.batch)
app.post('/main.php/download/getUrl', mparser.none(), download.getUrl)

app.post('/main.php/api', mparser.none(), mainapi.api)

app.post('/main.php/ranking/live', mparser.none(), live.ranking)
app.post('/main.php/ranking/player', mparser.none(), live.ranking)
app.post('/main.php/unit/deck', mparser.none(), customize.unit_deck)
app.post('/main.php/unit/deckName', mparser.none(), customize.unit_deckName)
app.post('/main.php/unit/setDisplayRank', mparser.none(), customize.unit_setDisplayRank)
app.post('/main.php/live/partyList', live.partyList)
app.post('/main.php/live/preciseScore', live.preciseScore)
app.post('/main.php/live/play', mparser.none(), live.play)
app.post('/main.php/live/reward', mparser.none(), live.reward)

app.post('/main.php/notice/noticeFriendGreeting', notice.FriendGreeting)
app.post('/main.php/notice/noticeFriendVariety', notice.FriendVariety)
app.post('/main.php/notice/noticeUserGreetingHistory', mparser.none(), notice.UserGreetingHistory)

app.post('/main.php/award/set', mparser.none(), customize.award_set)
app.post('/main.php/background/set', mparser.none(), customize.background_set)
app.post('/main.php/profile/profileRegister', mparser.none(), customize.profileRegister)

app.post('/main.php/area/list', misc.area_list)
app.post('/main.php/announce/checkState', misc.announce_checkState)
app.post('/main.php/personalnotice/get', misc.personalnotice_get)
app.post('/main.php/event/eventList', misc.event_eventList)
app.post('/main.php/payment/productList', misc.payment_productList)
app.post('/main.php/payment/month', misc.payment_month)
app.post('/main.php/exchange/itemInfo', misc.exchange_itemInfo)
app.post('/main.php/album/seriesAll', misc.album_seriesAll)
app.post('/main.php/museum/info', misc.museum_info)
app.post('/main.php/banner/bannerList', misc.bannerList)
app.post('/main.php/secretbox/pon', mparser.none(), gacha.gachapon)
app.post('/main.php/secretbox/multi', mparser.none(), gacha.gachapon)
app.post('/main.php/scenario/reward', mparser.none(), story.reward)
app.post('/main.php/subscenario/reward', mparser.none(), story.reward)
app.post('/main.php/multiunit/scenarioReward', mparser.none(), story.reward)
app.post('/main.php/reward/rewardHistory', misc.reward_rewardHistory)

app.get('/resources/maintenace/maintenance.php', function (req, res) {
	common_data.maintenance = false
	res.redirect(307, '/readme.html')
})

app.get('/*', function (req, res) {
	console.log("---------- UNHANDLED request: GET:", req.url)
	res.redirect(307, 'https://www.school-fes.klabgames.net/')
})

app.post('/*', mparser.none(), function (req, res) {
	console.log("---------- UNHANDLED request: POST:", req.url, '\n', req.body)
	util.stub_response(req, res)
})

migration.run()

app.listen(port, ip, callback = () => {
	let url = 'http://' + ip + ':' + port
	console.log('Server is listening at', url)
	console.log('Check', url + '/readme.html for info')
})
