const crypto = require('crypto');
const fs = require('fs')
const common_data = require('../data/common')

const privkey_path = 'priv.pem'
var privkey = undefined
const stub_resp = '{"response_data":[],"release_info":' + common_data.rel_info + ' ,"status_code":200}'

function timestamp() {
	return Math.floor(Date.now() / 1000)
}

function stub_response(req, res) {
    sign(req, res, stub_resp)
    res.send(stub_resp)
}

function sign(req, res, body) {
    let msgCode = ''
	res.setHeader('version_up', '0')
	res.setHeader('status_code', '200')
	res.setHeader('server_version', '20120129')
	res.setHeader('Server-Version', '59.4')
	res.setHeader('Authorize', 'consumerKey=lovelive_test&timeStamp=' + timestamp() + '&version=1.1&nonce=1')
    
	if (req.header('x-message-code') != undefined) {
        if (privkey == undefined) {
            privkey = fs.readFileSync(privkey_path)
        }
        msgCode = body + req.header('x-message-code')
		let msgSign = crypto.sign('sha1', Buffer.from(msgCode, 'utf-8'), {key: privkey}).toString('base64')
		res.setHeader('X-Message-Sign', msgSign)
	}
}

exports.stub_resp = stub_resp
exports.stub_response = stub_response
exports.timestamp = timestamp
exports.sign = sign
