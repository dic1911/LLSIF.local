const util = require('./util')
const common_data = require('../data/common')

let nid = 3846630165
// type
// 4, 5: friend req
// 6, 7: main/side story
// 11: gacha
// 12: kizuna max
// 13: lv max
// 14: chart clear
// 15: chart FC
// 16: chart score S
// 17: chart goal completed
// 18: event goal completed
// 19: event pt reached
// 20: another story

function varietyNotice(type, msg, time_str) {
	let center_unit_info = common_data.getUnit(common_data.extraUserInfo.navi_id)
	center_unit_info.removable_skill_ids = []
	return {
		"notice_id": nid,
		"new_flag": true,
		"reference_table": 0,
		"filter_id": 0,
		"notice_template_id": type,
		"message": msg,
		"readed": true,
		"insert_date": time_str,
		"affector": {
			"user_data": {
				"user_id": 16156098,
				"name": "You",
				"level": 180
			},
			"center_unit_info": center_unit_info,
			"setting_award_id": 77
		}
	}
}

function noticeFriend(req, res) {
	let ret = '{"response_data":{"item_count":0,"next_id":0,"notice_list":[],"server_timestamp":' + util.timestamp() + '},"release_info":' + common_data.rel_info + ',"status_code":200}'
	util.sign(req, res, ret)
	res.send(ret)
}

exports.FriendGreeting = noticeFriend

exports.FriendVariety = function (req, res) {
	let log = common_data.activityLog
	let reordered = log.slice(1).reverse().slice(0, 100).concat(common_data.activityLog[0])
	let list = []
	for (act of reordered)
		list.push(varietyNotice(act.type, act.message, act.time))
	let ret = '{"response_data":{"item_count":0,"next_id":0,"notice_list":' + JSON.stringify(list) + ',"server_timestamp":' + util.timestamp() + '},"release_info":' + common_data.rel_info + ',"status_code":200}'
	util.sign(req, res, ret)
	res.send(ret)
}

exports.UserGreetingHistory = function (req, res) {
	let ret = '{"response_data":{"item_count":0,"has_next":false,"notice_list":[],"server_timestamp":' + util.timestamp() + '},"release_info":' + common_data.rel_info + ',"status_code":200}'
	util.sign(req, res, ret)
	res.send(ret)
}
