const fs = require('fs')
const util = require('./util')
const common_data = require('../data/common')
const game_consts = require('../data/game_consts')
const e = require('express')

// const connection_config_pack_dir = 'http://127.0.0.1:51376/'
// const connection_config_pack_size = 1452

if (game_consts.useNPPS4Structure)
{

console.log("NPPS4 file structure is used as CDN!")

function npps4GetSinglePackage(host, os, pkgtype, pkgid) {
	const path = `/archive-root/${os}/package/59.4/${pkgtype}/${pkgid}/`
	const filePath = 'http' + path

	if (!fs.existsSync(filePath)) {
		return null;
	}

	const infov2 = JSON.parse(fs.readFileSync(filePath + 'infov2.json'))
	let result = []
	for (const info of infov2) {
		result.push({
			"url": host + path + info.name,
			"size": info.size.toString()
		})
	}

	return result
}

function npps4GetPackages(os, pkgtype, exclude) {
	const infojson = `/archive-root/${os}/package/59.4/${pkgtype}/info.json`

	if (!fs.existsSync('http' + infojson)) {
		return null
	}

	let result = []
	const info = JSON.parse(fs.readFileSync('http' + infojson))

	info.forEach(pkgid => {
		const intPkgId = parseInt(pkgid)
		if (!exclude.includes(intPkgId)) {
			result.push(intPkgId)
		}
	});

	return result
}

function npps4GetUpdate(host, os, ver) {
	const path = `/archive-root/${os}/update/${ver}/`
	const infov2 = JSON.parse(fs.readFileSync('http' + path + 'infov2.json'))

	let result = []
	for (const info of infov2) {
		result.push({
			"url": host + path + info.name,
			"size": info.size.toString(),
			"version": ver
		})
	}

	return result
}

function npps4ResolveMicroDL(host, rawOs, paths) {
	const os = rawOs.toLowerCase() == "android" ? "Android" : "iOS"
	const basePath = `/archive-root/${os}/package/59.4/microdl/`
	let result = []

	for (const path of paths) {
		result.push(host + basePath + path)
	}

	return result
}

function getDLResponse(host, download_type, req_body) {
	let osRequest = (req_body.target_os != undefined ? req_body.target_os : req_body.os)
	const os = osRequest.toLowerCase() == 'android' ? 'Android' : 'iOS'

	switch (download_type) {
		case "additional": {
			const result = npps4GetSinglePackage(host, os, req_body.package_type, req_body.package_id)

			if (result === null) {
				console.warn("download additional", req_body.package_type, req_body.package_id, "not found, returning 4501...")
				return '{"error_code":4501}'
			}

			return JSON.stringify(result)
		}
		case "batch": {
			const fileList = npps4GetPackages(os, req_body.package_type, req_body.excluded_package_ids ?? [])

			if (fileList == null) {
				console.warn("download batch", req_body.package_type, "not found, returning 4501...")
				return '{"error_code":4501}'
			}

			let result = []
			for (const pkgid of fileList) {
				const packages = npps4GetSinglePackage(host, os, req_body.package_type, pkgid) ?? []
				result.push(...packages)
			}

			return JSON.stringify(result)
		}
		case "update": {
			const basePath = `/archive-root/${os}/update/`
			const packageList = JSON.parse(fs.readFileSync('http' + basePath + 'infov2.json'))
			const packageListTuple = []

			for (const packageNumber of packageList) {
				const splitted = packageNumber.split('.', 2)
				packageListTuple.push([parseInt(splitted[0]), parseInt(splitted[1])])
			}

			const currentVersionSplit = (req_body.external_version ?? req_body.install_version).split(".", 2)
			const currentVersion = [parseInt(currentVersionSplit[0]), parseInt(currentVersionSplit[1])]

			let result = []
			for (const version of packageListTuple) {
				if (version > currentVersion) {
					result.push(...npps4GetUpdate(host, os, version.join(".")))
				}
			}

			let injectInfoSize = 0
			let injectInfoUrl = ''
			if (fs.existsSync('./http/server_info') && game_consts.server_info_size == 0) {
				injectInfoSize = fs.lstatSync('./http/server_info').size
				injectInfoUrl = game_consts.cdn_path + 'server_info'
			} else if (game_consts.server_info_url.length > 0 && game_consts.server_info_size > 0) {
				injectInfoSize = game_consts.server_info_size
				injectInfoUrl = game_consts.server_info_url
			}

			if (injectInfoSize > 0) {
				console.log("serving additional server_info", injectInfoUrl)
				result.push({
					"size": injectInfoSize,
					"url": injectInfoUrl,
					"version": "59.4"
				})
			}

			return JSON.stringify(result)
		}
		default: {
			console.warn("download type", download_type, "not found, returning 4501...")
			return '{"error_code":4501}'
		}
	}
}

} else {

function getDLResponse(host, download_type, req_body) {
	let os = (req_body.target_os != undefined ? req_body.target_os : req_body.os)
	if (os.toLowerCase() == 'ios') os = 'iphone'
	let basedir = 'static/download_targets/' + os + '/' + download_type + '/'
	let path = basedir
	switch (download_type) {
		case "additional": {
			basedir = basedir + req_body.package_type + '/'
			path = basedir + req_body.package_id + '.json'
			break
		}
		case "batch": {
			if (req_body.package_type == 4 && game_consts.workaroundUnitBatchDL) return '[]'

			basedir = basedir + req_body.package_type + '/'
			path = basedir + (req_body.package_type != 4 ? 'all.json' : 'all-out.json')
			break
		}
		case "update": {
			path = basedir + req_body.install_version + '.json'
			break
		}
	}
	if (!fs.existsSync(path)) {
		console.warn("download target", path, "not found, returning empty array...")
		return '[]'
	}

	let blacklist = []
	if (req_body.excluded_package_ids != undefined && typeof req_body.excluded_package_ids[Symbol.iterator] == 'function') {
		for (id of req_body.excluded_package_ids) {
			let ppath = basedir + id + '.json'
			let pdata = JSON.parse(fs.readFileSync(ppath, 'utf8'))
			for (p of pdata) {
				let fn = p.url.split('/').reverse()[0].split('?')[0].split('.')[0]
				blacklist.push(fn)
			}
		}
	}

	let filtered_data = []
	let data = JSON.parse(fs.readFileSync(path, 'utf8'))
	for (i in data) {
		let newUrl = data[i].url.replace(game_consts.official_cdn_domain, game_consts.cdn_path)
		let fn1 = newUrl.split('/').reverse()[0]
		let fn2 = fn1.split('?')[0].split('.')[0]

		if (blacklist.indexOf(fn2) > -1) continue

		data[i].url = newUrl.replace(fn1, fn2)
		filtered_data.push(data[i])
	}
	if (download_type == "update") {
		let size = 0
		let url = ''
		if (fs.existsSync('./http/server_info') && game_consts.server_info_size == 0) {
			size = fs.lstatSync('./http/server_info').size
			url = game_consts.cdn_path + 'server_info'
		} else if (game_consts.server_info_url.length > 0 && game_consts.server_info_size > 0) {
			size = game_consts.server_info_size
			url = game_consts.server_info_url
		}

		if (size > 0) {
			console.log("serving additional server_info", url)
			filtered_data.push({
				"size": size,
				"url": url,
				"version": "59.4"
			})
		}
	}
	return JSON.stringify(filtered_data)
}

}

function getHostPort(req) {
	return req.protocol + "://" + req.headers.host
}

exports.update = function (req, res) {
	console.log("download.update")
	if (common_data.maintenance) {
		res.setHeader('Maintenance', 1)
	}
	const body = JSON.parse(req.body.request_data)
	ret = common_data.maintenance ? ' ' : '{"response_data":' + getDLResponse(getHostPort(req), 'update', body) + ',"release_info":' + common_data.rel_info + ',"status_code":200}'
	util.sign(req, res, ret)
	res.send(ret)
}

exports.event = function (req, res) {
	console.log("download.event")
	if (common_data.maintenance) {
		res.setHeader('Maintenance', 1)
	}
	ret = common_data.maintenance ? ' ' : util.stub_resp
	util.sign(req, res, ret)
	res.send(ret)
}

exports.additional = function (req, res) {
	const body = JSON.parse(req.body.request_data)
	console.log("download.additional", body)
	let data = getDLResponse(getHostPort(req), "additional", body)
	let ret = '{"response_data":' + data + ',"release_info":' + common_data.rel_info + ',"status_code":200}'
	util.sign(req, res, ret)
	res.send(ret)
}

exports.batch = function (req, res) {
	console.log('download.batch', req.body)
	const body = JSON.parse(req.body.request_data)
	let ret = '{"response_data":' + getDLResponse(getHostPort(req), "batch", body) + ',"release_info":' + common_data.rel_info + ',"status_code":200}'
	util.sign(req, res, ret)
	res.send(ret)
}

exports.getUrl = function(req, res) {
	let body = JSON.parse(req.body.request_data)
	let ver = '59.4'
	if (req.header('Client-Version') != undefined) {
		ver = req.header('Client-Version')
	}

	let list = null
	if (game_consts.useNPPS4Structure) {
		list = npps4ResolveMicroDL(getHostPort(req), body.os, body.path_list)
	} else {
		let base_dir = game_consts.microdl_dir.replace('<OS>', (game_consts.osToLowerCase ? body.os.toLowerCase() : body.os)).replace("<VER>", ver)
		list = []
		for (x of body.path_list) {
			let link = (base_dir + x)
			console.log('getUrl:', link)
			list.push(link)
		}
	}

	let ret_json = {
		response_data: {
			url_list: list
		},
		release_info: JSON.parse(common_data.rel_info),
		status_code: 200
	}
	let ret = JSON.stringify(ret_json)
	util.sign(req, res, ret)
	res.send(ret)
}
