const util = require('./util')
const common_data = require('../data/common')

exports.userInfo = function (req, res) {
	console.log("user.userInfo", req.body)
	let userInfo = common_data.userInfo
	userInfo.server_timestamp = util.timestamp()
	let ret_json = {
		response_data: userInfo,
		release_info: JSON.parse(common_data.rel_info),
		status_code: 200
	}
	let ret = JSON.stringify(ret_json)
	util.sign(req, res, ret)
	res.send(ret)
}

exports.changeName = function (req, res) {
	console.log("change name", req.body.request_data)
	let body = JSON.parse(req.body.request_data)
	let userInfo = common_data.userInfo
	let ret = '{"response_data":{"before_name":"' + userInfo.user.name + '","after_name":"' + body.name + '","server_timestamp":' + util.timestamp() + '},"release_info":' + common_data.rel_info + ',"status_code":200}'
	userInfo.user.name = body.name
	common_data.saveUserInfo(userInfo)
	util.sign(req, res, ret)
	res.send(ret)
}

exports.changeNavi = function (req, res) {
	console.log("change navi", req.body.request_data)
	let body = JSON.parse(req.body.request_data)
	let info = common_data.extraUserInfo
	info.navi_id = body.unit_owning_user_id
	common_data.saveExtraUserInfo(info)
	util.stub_response(req, res)
}
