const common_data = require('../data/common')
const game_consts = require('../data/game_consts')
const util = require('./util')

exports.reward = function (req, res) {
	let body = JSON.parse(req.body.request_data)
	let userInfo = common_data.userInfo
	let type = "scenario"
	let id = body.scenario_id
	if (id == undefined) {
		type = "subscenario"
		id = body.subscenario_id
	}
	if (id == undefined) {
		type = "multi_unit_scenario"
		id = body.multi_unit_scenario_id
	}

	let ret_json = {
		"response_data": {
			"before_user_info": userInfo.user,
			"after_user_info": userInfo.user,
			"next_level_info": {
				"level": userInfo.user.level,
				"from_exp": userInfo.user.exp,
			},
			"base_reward_info": {
				"game_coin_reward_box_flag": false,
				"game_coin": 20000
			},
			"item_reward_info": [],
			"class_system": common_data.classSystem,
			"museum_info": JSON.parse(game_consts.museum_info_txt),
			"server_timestamp": util.timestamp(),
			"present_cnt": 0
		},
		"release_info": JSON.parse(common_data.rel_info),
		"status_code": 200

	}
	ret_json.response_data["clear_" + type] = {}
	ret_json.response_data["clear_" + type][type + "_id"] = id

	let ret = JSON.stringify(ret_json)
	console.log(ret)
	util.sign(req, res, ret)
	res.send(ret)
}
