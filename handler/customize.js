const util = require('./util')
const common_data = require('../data/common')

exports.award_set = function (req, res) {
	let body = JSON.parse(req.body.request_data)
	let info = common_data.extraUserInfo
	info.award_id = (body.award_id == 10000 ? (10000 + common_data.extraUserInfo.rank) : body.award_id)
	common_data.saveExtraUserInfo(info)
	util.stub_response(req, res)
}

exports.background_set = function (req, res) {
	let body = JSON.parse(req.body.request_data)
	let info = common_data.extraUserInfo
	info.bg_id = body.background_id
	common_data.saveExtraUserInfo(info)
	util.stub_response(req, res)
}

exports.profileRegister = function (req, res) {
	let body = JSON.parse(req.body.request_data)
	let info = common_data.extraUserInfo
	info.bio = body.introduction
	common_data.saveExtraUserInfo(info)
	util.stub_response(req, res)
}

exports.unit_deck = function (req, res) {
	let body = JSON.parse(req.body.request_data)
	let newDeckInfo = []
	for (deck of body.unit_deck_list) {
		newDeckInfo.push({
			unit_deck_id: deck.unit_deck_id,
            main_flag: (deck.main_flag == 1),
            deck_name: deck.deck_name,
			unit_owning_user_ids: deck.unit_deck_detail
		})
	}
	common_data.updateDeckInfo(newDeckInfo)
	util.stub_response(req, res)
}

exports.unit_deckName = function (req, res) {
	let body = JSON.parse(req.body.request_data)
	let deckInfo = common_data.deckInfo
	for (i in deckInfo) {
		if (deckInfo[i].unit_deck_id == body.unit_deck_id && deckInfo[i].deck_name != body.deck_name) {
			deckInfo[i].deck_name = body.deck_name
			common_data.updateDeckInfo(deckInfo)
			break
		}
	}
	util.stub_response(req, res)
}

exports.unit_setDisplayRank = function (req, res) {
	let body = JSON.parse(req.body.request_data)
	common_data.updateUnitDisplayRank(body.unit_owning_user_id, body.display_rank)
	util.stub_response(req, res)
}
