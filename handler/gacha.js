const util = require('./util')
const common_data = require('../data/common')
const game_consts = require('../data/game_consts')

function getGachaCard() {
	let card = common_data.getRandomActiveUnit()
	// R: 2, UR: 4
	let rarity = 2
	if (card.max_level >= 100) {
		rarity = 4
	} else if (card.max_level >= 90) {
		rarity = 5
	} else if (card.max_level >= 80) {
		rarity = 3
	} else if (card.max_level >= 60) {
		rarity = 2
	} else {
		rarity = 1
	}

	return {
            "unit_rarity_id": rarity,
            "is_hit": null,
            "add_type": 1001,
            "amount": 1,
            "item_category_id": 0,
            "unit_id": card.unit_id,
            "unit_owning_user_id": card.unit_owning_user_id, //460618605,
            "is_support_member": false,
            "exp": card.exp,
            "next_exp": card.next_exp,
            "max_hp": card.max_hp,
            "level": card.level,
            "max_level": card.max_level,
            "level_limit_id": 0,
            "skill_level": card.unit_skill_level, //1,
            "rank": card.max_rank,
            "love": card.love,
            "is_rank_max": true,
            "is_level_max": false,
            "is_love_max": false,
            "is_signed": false,
            "new_unit_flag": true,
            "reward_box_flag": false,
            "unit_skill_exp": card.unit_skill_exp, //0,
            "display_rank": card.max_rank,
            "unit_removable_skill_capacity": card.unit_removable_skill_capacity,
            "removable_skill_ids": []
	}

}

exports.gachapon = function (req, res) {
    let body = JSON.parse(req.body.request_data)
    console.log("gacha req.", body, body.count)
    let box = {}
    for (g of game_consts.gachaPools.member_category_list) {
	for (b of g.page_list) {
		if (b.secret_box_info.secret_box_id == body.secret_box_id) {
			box = b
			break
		}
	}
        if (Object.entries(box).length != 0) break
    }
    let gachaResult = []
    for (let i = 0; i < ((body.count == undefined) ? 1 : body.count); i++) {
	gachaResult.push(getGachaCard())
    }
    let ret_json = {
        "response_data": {
            "is_unit_max": false,
            "item_list": (() => {
			let ret = []
			for (id of game_consts.gachaItemIds) {
				ret.push({ item_id: id, amount: 69 })
			}
			return ret
		})(),
            "gauge_info": {
                "max_gauge_point": 100,
                "gauge_point": box.secret_box_info.add_gauge,
                "added_gauge_point": box.secret_box_info.add_gauge
            },
            "button_list": box.button_list,
            "secret_box_info": Object.assign(box.secret_box_info, {
		pon_count: gachaResult.length - 1
	    }),
            "secret_box_items": {
                "unit": gachaResult,
                "item": []
            },
            "before_user_info": common_data.userInfo,
            "after_user_info": common_data.userInfo,
            "secret_box_badge_flag": false, // true
            "lowest_rarity": 1,
            "promotion_performance_rate": 10,
            "secret_box_parcel_type": 2,
            "museum_info": JSON.parse(game_consts.museum_info_txt),
            "server_timestamp": util.timestamp(),
            "present_cnt": 0
        },
        "release_info": JSON.parse(common_data.rel_info),
        "status_code": 200
    }
	let ret = JSON.stringify(ret_json)
	util.sign(req, res, ret)
	res.send(ret)
}
