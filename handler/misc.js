const fs = require('fs')
const util = require('./util')
const common_data = require('../data/common')

exports.personalnotice_get = function (req, res) {
	let ret = '{"response_data":{"has_notice":false,"notice_id":0,"type":0,"title":"","contents":"","server_timestamp":' + util.timestamp() + '},"release_info":' + common_data.rel_info + ',"status_code":200}'
	util.sign(req, res, ret)
	res.send(ret)
}

exports.event_eventList = function (req, res) {
	let ret = '{"response_data":{"target_list":[],"server_timestamp":' + util.timestamp() + '},"release_info":' + common_data.rel_info + ',"status_code":200}'
	util.sign(req, res, ret)
	res.send(ret)
}

exports.payment_productList = function (req, res) {
	let ret = '{"response_data":{"restriction_info":{"restricted":false},"under_age_info":{"birth_set":false,"has_limit":false,"limit_amount":null,"month_used":0},"sns_product_list":[],"product_list":[],"subscription_list":[],"show_point_shop":true},"release_info":' + common_data.rel_info + ',"status_code":200}'
	util.sign(req, res, ret)
	res.send(ret)
}

exports.payment_month = function (req, res) {
	let ret = '{"response_data":{"item_count":0,"payment_month_list":[],"server_timestamp":' + util.timestamp() +'},"release_info":' + common_data.rel_info + ',"status_code":200}'
	util.sign(req, res, ret)
	res.send(ret)
}

exports.exchange_itemInfo = function (req, res) {
	let ret = '{"response_data":{"exchange_item_info":[],"exchange_point_list":[],"server_timestamp":' + util.timestamp() + '},"release_info":' + common_data.rel_info + ',"status_code":200}'
	util.sign(req, res, ret)
	res.send(ret)
}

exports.album_seriesAll = function (req, res) {
	var albumInfo = JSON.parse(fs.readFileSync('static/album.seriesAll.response_data.json', 'utf8'))
	let ret_json = {
		response_data: albumInfo,
		release_info: JSON.parse(common_data.rel_info),
		status_code: 200
	}
	let ret = JSON.stringify(ret_json)
	util.sign(req, res, ret)
	res.send(ret)
}

exports.museum_info = function (req, res) {
	var museumInfo = JSON.parse(fs.readFileSync('static/museum_info.json', 'utf8'))
	let ret_json = {
		response_data: { museum_info: museumInfo },
		release_info: JSON.parse(common_data.rel_info),
		status_code: 200
	}
	let ret = JSON.stringify(ret_json)
	util.sign(req, res, ret)
	res.send(ret)
}

exports.bannerList = function (req, res) {
	let data = undefined
	if (common_data.cache.main_php_api.banner != undefined && common_data.cache.main_php_api.banner.bannerList != undefined) {
		data = common_data.cache.main_php_api.banner.bannerList
	} else {
		data = JSON.parse(fs.readFileSync('static/main.php-api/banner.bannerList.result.json', 'utf8'))
	}
	let ret_json = {
		response_data: data,
		release_info: JSON.parse(common_data.rel_info),
		status_code: 200
	}
	let ret = JSON.stringify(ret_json)
	util.sign(req, res, ret)
	res.send(ret)
}

exports.area_list = function (req, res) {
	let ret_json = {
		response_data: {
			secret_banner_flag: false,
			area_list: [],
			server_timestamp: util.timestamp()
		},
		release_info: JSON.parse(common_data.rel_info),
		status_code: 200
	}
	let ret = JSON.stringify(ret_json)
	util.sign(req, res, ret)
	res.send(ret)
}

exports.announce_checkState = function (req, res) {
	let ret_json = {
		response_data: {
			has_unread_announce: false,
			server_timestamp: util.timestamp()
		},
		release_info: JSON.parse(common_data.rel_info),
		status_code: 200
	}
	let ret = JSON.stringify(ret_json)
	util.sign(req, res, ret)
	res.send(ret)
}

exports.reward_rewardHistory = function (req, res) {
	let ret_json = {
		response_data: {"item_count":0,"limit":20,"history":[]},
		release_info: JSON.parse(common_data.rel_info),
		status_code: 200
	}
	let ret = JSON.stringify(ret_json)
	util.sign(req, res, ret)
	res.send(ret)
}
