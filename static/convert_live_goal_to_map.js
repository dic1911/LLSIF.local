const fs = require('fs')
const json = JSON.parse(fs.readFileSync('live_goal_reward_m.json', 'utf8'))

let ret = {}

// rank: 1 = S, 2 = A, 3 = B, 4 = C
// live_goal_type: 1: score, 2: combo, 3: clear
for (entry of json) {
    if (ret[entry.live_difficulty_id] == undefined) {
        ret[entry.live_difficulty_id] = {
            score: [],
            combo: [],
            clear: []
        }
    }
    
    if (entry.live_goal_type == 1)
        ret[entry.live_difficulty_id].score[entry.rank - 1] = entry.live_goal_reward_id
    else if (entry.live_goal_type == 2)
        ret[entry.live_difficulty_id].combo[entry.rank - 1] = entry.live_goal_reward_id
    else if (entry.live_goal_type == 3)
        ret[entry.live_difficulty_id].clear[entry.rank - 1] = entry.live_goal_reward_id
}

fs.writeFileSync('live_goal_reward_map.json', JSON.stringify(ret))