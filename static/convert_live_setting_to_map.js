const fs = require('fs')
const json = JSON.parse(fs.readFileSync('live_setting_m.json', 'utf8'))
const track_json = JSON.parse(fs.readFileSync('live_track_m.json', 'utf8'))

let _clear_rank = [
	[20, 10, 5, 1],
	[40, 20, 10, 5],
	[100, 40, 20, 10],
	[120, 60, 30, 15],
	[150, 60, 30, 15]
]
_clear_rank.push(_clear_rank[4])

let map = {}
let track_map = {}

for (track of track_json) {
    track_map[track.live_track_id] = track.name + " (" + track.name_en + ")"
}

for (chart of json) {
    map[chart.notes_setting_asset] = {
        ac_flag: chart.ac_flag,
        clear_rank: _clear_rank[chart.difficulty - 1],
        combo_rank: [chart.s_rank_combo, chart.a_rank_combo, chart.b_rank_combo, chart.c_rank_combo],
        difficulty: chart.difficulty,
        score_rank: [chart.s_rank_score, chart.a_rank_score, chart.b_rank_score, chart.c_rank_score],
        swing_flag: chart.swing_flag,
        track_name: track_map[chart.live_track_id]
    }
}

fs.writeFileSync('live_setting_map.json', JSON.stringify(map))
